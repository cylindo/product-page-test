# Cylindo Programming test

## Assigment

For this test we would like you to implement a product page for our fake furniture brand:

![Furnit](./furnit_logo.svg)

The product page should include the 360 viewer and a right panel for selecting features.

here are some examples for inspiration:

- [Interior Define](https://www.interiordefine.com/shop/ms-chesterfield-accent-chair#NT-3385-216A/Leg016-1)
- [Ethan Allen](https://www.ethanallen.com/en_US/shop-furniture-bedroom-night-tables/alec-night-table/385516.html?dwvar_385516_finish=506#start=1)

We are looking for:

- A functional product page where you can select product variations in the right panel and see them in the 360 Viewer
- Responsive design that works well on mobile and large screens
- Semantic markup
- Code that works in all mordern browsers (exclude ie / safari depending on your development environment)

## Implementation

Use [index.html](./index.html) as a starting point.

Refer to the [360 Viewer docs](http://clients.cylindo.com/viewer/3.x/v3.2/documentation/#/viewer-intro) in order to implement the viewer.

[product.json](./product.json) contains the relevant information about the product.

Its important that you can select all combination of features available for the product.

## Delivery

When you are done send us a zip archive of the folder. The page should work if we open index.html in a browser.
